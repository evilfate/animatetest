package fate.com.animatetest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final LinearInterpolator linearInterpolator = new LinearInterpolator();
    int animatedCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final View dot1 = findViewById(R.id.dot1);
        final View dot2 = findViewById(R.id.dot2);
        final View dot3 = findViewById(R.id.dot3);
        final View dot4 = findViewById(R.id.dot4);
    }


    private void startDot1Animation() {
        final View dot1 = findViewById(R.id.dot1);
        Animation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, -2.50f);

        mAnimation.setDuration(166);
        mAnimation.setInterpolator(linearInterpolator);
//        mAnimation.setFillAfter(true);
        mAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation mAnimation = new TranslateAnimation(
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.RELATIVE_TO_SELF, -2.5f,
                        TranslateAnimation.RELATIVE_TO_SELF, 0f);

                mAnimation.setDuration(166);
                mAnimation.setInterpolator(linearInterpolator);
//                mAnimation.setFillAfter(true);
                dot1.startAnimation(mAnimation);

                startDot2Animation(animation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        dot1.startAnimation(mAnimation);
    }

    private void startDot2Animation(Animation animation) {
        final View dot2 = findViewById(R.id.dot2);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation mAnimation = new TranslateAnimation(
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.RELATIVE_TO_SELF, -2.5f,
                        TranslateAnimation.RELATIVE_TO_SELF, 0f);

                mAnimation.setDuration(166);
                mAnimation.setInterpolator(linearInterpolator);
//                mAnimation.setFillAfter(true);
                dot2.startAnimation(mAnimation);

                startDot3Animation(animation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        dot2.startAnimation(animation);
    }

    private void startDot3Animation(Animation animation) {
        final View dot3 = findViewById(R.id.dot3);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation mAnimation = new TranslateAnimation(
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.RELATIVE_TO_SELF, -2.5f,
                        TranslateAnimation.RELATIVE_TO_SELF, 0f);

                mAnimation.setDuration(166);
                mAnimation.setInterpolator(linearInterpolator);
//                mAnimation.setFillAfter(true);
                dot3.startAnimation(mAnimation);

                startDot4Animation(animation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        dot3.startAnimation(animation);
    }

    private void startDot4Animation(Animation animation) {
        final View dot4 = findViewById(R.id.dot4);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation mAnimation = new TranslateAnimation(
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.ABSOLUTE, 0f,
                        TranslateAnimation.RELATIVE_TO_SELF, -2.5f,
                        TranslateAnimation.RELATIVE_TO_SELF, 0f);

                mAnimation.setDuration(166);
                mAnimation.setInterpolator(linearInterpolator);
//                mAnimation.setFillAfter(true);
                dot4.startAnimation(mAnimation);

                if (animatedCount < 2) {
                    animatedCount++;
                    startDot1Animation();
                } else {
                    findViewById(R.id.dot1).setVisibility(View.GONE);
                    findViewById(R.id.dot2).setVisibility(View.GONE);
                    findViewById(R.id.dot3).setVisibility(View.GONE);
                    findViewById(R.id.dot4).setVisibility(View.GONE);
                    TextView textView = (TextView) findViewById(R.id.textView);
                    textView.setVisibility(View.VISIBLE);
                    Animation textAnimation = new TranslateAnimation(
                            TranslateAnimation.RELATIVE_TO_SELF, 1f,
                            TranslateAnimation.RELATIVE_TO_SELF, 0f,
                            TranslateAnimation.RELATIVE_TO_SELF, 0f,
                            TranslateAnimation.RELATIVE_TO_SELF, 0f);

                    textAnimation.setDuration(300);
                    textAnimation.setStartOffset(300);
                    textAnimation.setInterpolator(linearInterpolator);
//                    textAnimation.setFillAfter(true);
                    textView.startAnimation(textAnimation);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        dot4.startAnimation(animation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            animatedCount = 0;
            findViewById(R.id.dot1).setVisibility(View.VISIBLE);
            findViewById(R.id.dot2).setVisibility(View.VISIBLE);
            findViewById(R.id.dot3).setVisibility(View.VISIBLE);
            findViewById(R.id.dot4).setVisibility(View.VISIBLE);
            TextView textView = (TextView) findViewById(R.id.textView);
            textView.setVisibility(View.INVISIBLE);
            startDot1Animation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
